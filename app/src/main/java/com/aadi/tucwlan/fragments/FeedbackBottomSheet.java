package com.aadi.tucwlan.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;

import com.aadi.tucwlan.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class FeedbackBottomSheet extends BottomSheetDialogFragment {

    private BottomSheetBehavior<View> mBehavior;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final BottomSheetDialog sheet = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        final View view = View.inflate(getContext(), R.layout.fragment_bottom_sheet_feedback, null);

        sheet.setContentView(view);
        mBehavior = BottomSheetBehavior.from((View) view.getParent());
        mBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);
        mBehavior.setSkipCollapsed(true);

        ((AppCompatButton) view.findViewById(R.id.button_title)).setText(getResources().getString(R.string.btn_finish));
        ((AppCompatButton) view.findViewById(R.id.button_title)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        ((TextView) view.findViewById(R.id.logged_in_as)).setText(getResources().getString(R.string.msg_feedback_title));


        return sheet;
    }

}
