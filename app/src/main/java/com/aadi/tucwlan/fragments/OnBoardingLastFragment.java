package com.aadi.tucwlan.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aadi.tucwlan.R;
import com.aadi.tucwlan.utils.Helper;

public class OnBoardingLastFragment extends Fragment {

    public static Boolean isConnected = false;
    public static Boolean isSecure = false;
    public static Boolean hasLocationPermission = false;
    private ImageView checkbox1;
    private ImageView checkbox2;
    private ImageView checkbox3;

    private NetworkChangeReceiver networkChangeReceiver = null;
    private LinearLayout location_check_layout;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LinearLayout mRootView = (LinearLayout) inflater.inflate(R.layout.fragment_onboarding_last, container, false);

        checkbox1 = (ImageView) mRootView.findViewById(R.id.checkbox1);
        checkbox2 = (ImageView) mRootView.findViewById(R.id.checkbox2);

        ((LinearLayout) mRootView.findViewById(R.id.internet_check_layout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isConnected){
                   Helper.showCheckInternetDialog(v);
                }
            }
        });

        ((LinearLayout) mRootView.findViewById(R.id.lock_screen_layout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isSecure){
                    Helper.showCheckLockScreenDialog(v);
                }
            }
        });

        updateLayout();

        return mRootView;

    }

    private void updateLayout(){

        if(Helper.hasLockScreenSet(getContext())){
            checkbox1.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_box));
            isSecure = true;

        } else {
            checkbox1.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_box_outline_blank));
            isSecure = false;
        }

        if(Helper.hasInternet(getContext())){
            checkbox2.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_box));
            isConnected = true;

        } else {
            checkbox2.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_box_outline_blank));
            isConnected = false;
        }

    }

    @Override
    public void onPause() {
        super.onPause();
    }



    public static Fragment newInstance() {
        return new OnBoardingLastFragment();
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().getApplicationContext().unregisterReceiver(this.networkChangeReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateLayout();

        IntentFilter networkChangeReceiverIntentFilter = new IntentFilter();
        networkChangeReceiverIntentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        networkChangeReceiverIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        this.networkChangeReceiver = new NetworkChangeReceiver();
        Log.d("networkChange_broadcast", "register receiver");
        getActivity().getApplicationContext().registerReceiver(this.networkChangeReceiver, networkChangeReceiverIntentFilter);
    }

    private class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d("networkChange_broadcast", "Get action: " + intent.getAction());
                updateLayout();
            } catch (Exception exception) {
                Log.d("networkChange_broadcast", "Error while recompute ticklist", exception);
            }
        }
    }
}
