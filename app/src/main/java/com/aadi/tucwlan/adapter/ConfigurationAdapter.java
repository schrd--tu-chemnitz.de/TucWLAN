package com.aadi.tucwlan.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aadi.tucwlan.R;
import com.aadi.tucwlan.activity.InstallingConfiguration;
import com.aadi.tucwlan.model.Configuration;

import java.util.List;

public class ConfigurationAdapter extends RecyclerView.Adapter<ConfigurationAdapter.ViewHolder> {

    private List<Configuration> list;

    public ConfigurationAdapter(Context context, List<Configuration> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_configuration_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Configuration config = list.get(getItemCount() - 1  - position);

        holder.deviceName.setText(config.getDeviceName());
        holder.lastUsed.setText(config.getLastUsed());
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Context context = v.getContext();
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle(R.string.title_reset_config);
                builder.setMessage(R.string.msg_reset_config);
                builder.setPositiveButton(R.string.YES, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent= new Intent(context, InstallingConfiguration.class);
                        intent.putExtra("NEW_CONFIGURATION", false);
                        intent.putExtra("DEVICE_NAME", config.getDeviceName());
                        intent.putExtra("PRIMARY_KEY", config.getPk());
                        intent.putExtra("RESET_URI" , config.getResetUri());
                        context.startActivity(intent);
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final CardView card_view;
        TextView deviceName, lastUsed;

        ViewHolder(View itemView) {
            super(itemView);

            card_view = itemView.findViewById(R.id.card_view);
            deviceName = itemView.findViewById(R.id.device_name);
            lastUsed = itemView.findViewById(R.id.last_used);

        }
    }
}

