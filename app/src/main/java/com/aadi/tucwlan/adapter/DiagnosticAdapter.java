package com.aadi.tucwlan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aadi.tucwlan.R;
import com.aadi.tucwlan.model.TickItems;
import com.aadi.tucwlan.utils.Helper;

import java.util.List;

public class DiagnosticAdapter extends RecyclerView.Adapter<DiagnosticAdapter.ViewHolder> {

    private List<TickItems> list;
    private Context mContext;

    public DiagnosticAdapter(Context context, List<TickItems> list) {
        this.list = list;
        this.mContext = context;
    }

    @NonNull
    @Override
    public DiagnosticAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LinearLayout view = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tick_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DiagnosticAdapter.ViewHolder holder, int position) {

        final TickItems mTickItem = list.get(position);
        if(mTickItem.getTitle()!= null){
            holder.card.setVisibility(View.VISIBLE);
            holder.title.setText(mTickItem.getTitle());
        } else
            holder.card.setVisibility(View.GONE);

        holder.subtitle.setText(mTickItem.getSubtitle());

        if(mTickItem.isCorrect())
            holder.icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_check_circle));
        else
            holder.icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_error));

        if(mTickItem.getSection_name()!= null){
            if(mTickItem.getSection_name().contains("space")) {
                holder.secion_name.setText(" ");
            } else {
                holder.secion_name.setText(mTickItem.getSection_name());
            }
            holder.section_layout.setVisibility(View.VISIBLE);
        } else {
            holder.section_layout.setVisibility(View.GONE);
        }


        if(mTickItem.isClickable()){
            holder.action_icon.setVisibility(View.VISIBLE);
        } else
            holder.action_icon.setVisibility(View.GONE);

        if(mTickItem.getAction()!= null) {
            holder.card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getClickAction(mTickItem.getAction(), view);
                }
            });
        }

    }

    private void getClickAction(String action, View view) {
        if(action.contains("location_dialog_box"))
            Helper.showLocationPermissionDialog(mContext);
        else if(action.contains("internet_dialog_box"))
            Helper.showCheckInternetDialog(mContext);
        else if(action.contains("screen_lock_dialog_box"))
            Helper.showCheckLockScreenDialog(view);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final AppCompatTextView title, subtitle;
        private final ImageView icon;
        private final LinearLayout section_layout;
        private final AppCompatTextView secion_name;
        private final ImageView action_icon;
        private final CardView card;

        ViewHolder(View itemView) {
            super(itemView);

            card = (CardView) itemView.findViewById(R.id.item_tick_view_card_layout);
            title = (AppCompatTextView) itemView.findViewById(R.id.item_tick_title);
            subtitle = (AppCompatTextView) itemView.findViewById(R.id.item_tick_subtitle);
            icon = (ImageView) itemView.findViewById(R.id.item_tick_icon);
            action_icon = (ImageView) itemView.findViewById(R.id.item_tick_action);

            section_layout = (LinearLayout) itemView.findViewById(R.id.section_name_layout);
            secion_name = (AppCompatTextView) itemView.findViewById(R.id.section_name);

        }
    }
}
