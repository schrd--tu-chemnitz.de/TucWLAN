package com.aadi.tucwlan.model;

import com.google.gson.annotations.SerializedName;

public class Configuration {

    @SerializedName("device_name")
    private String deviceName;

    @SerializedName("last_used")
    private String lastUsed;

    @SerializedName("reset_uri")
    private String resetUri;

    @SerializedName("deploy_state")
    private int deployState;

    @SerializedName("state")
    private int state;

    @SerializedName("pk")
    private int pk;


    public Configuration() {
    }

    public Configuration(String deviceName, String lastUsed, String resetUri, int deployState, int state, int pk) {
        this.deviceName = deviceName;
        this.lastUsed = lastUsed;
        this.resetUri = resetUri;
        this.deployState = deployState;
        this.state = state;
        this.pk = pk;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(String lastUsed) {
        this.lastUsed = lastUsed;
    }

    public String getResetUri() {
        return resetUri;
    }

    public void setResetUri(String resetUri) {
        this.resetUri = resetUri;
    }

    public int getDeployState() {
        return deployState;
    }

    public void setDeployState(int deployState) {
        this.deployState = deployState;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }
}
