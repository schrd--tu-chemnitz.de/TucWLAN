package com.aadi.tucwlan.model;

public class TickItems {

    private String title;
    private String subtitle;
    private boolean isCorrect;
    private boolean isClickable;
    private String action;
    private String section_name;

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public TickItems(String title, String subtitle, boolean isCorrect, boolean isClickable, String action, String section_name) {
        this.title = title;
        this.subtitle = subtitle;
        this.isCorrect = isCorrect;
        this.isClickable = isClickable;
        this.action = action;
        this.section_name = section_name;
    }

    public TickItems(String section_name) {
        this.section_name = section_name;
    }

    public TickItems(String title, String subtitle, boolean isCorrect) {
        this.title = title;
        this.subtitle = subtitle;
        this.isCorrect = isCorrect;
    }

    public TickItems(String title, String subtitle, boolean isCorrect, String section_name) {
        this.title = title;
        this.subtitle = subtitle;
        this.isCorrect = isCorrect;
        this.section_name = section_name;
    }

    public TickItems() {
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public void setClickable(boolean clickable) {
        isClickable = clickable;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
