package com.aadi.tucwlan.utils;

import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.aadi.tucwlan.Application;
import com.aadi.tucwlan.R;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URI;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

public class Helper {

    public static String API_USER_URL = "https://idm.hrz.tu-chemnitz.de/js/current_user/";
    public static String API_URL = "https://idm.hrz.tu-chemnitz.de/user/service/account/devicepasswords/api/"; // PRODUCTION

    public static final String SHARED_PREF_FILE = "TUCWLAN_settings";
    public static final String SHARED_PREF_NEW_USER = "NEW_USER";
    public static final String SHARED_PREF_DEVICE_NAME = "device_name";
    public static final String SHARED_PREF_LAST_ADDED_WIFI = "lastAddedWifi";
    public static final String SHARED_PREF_INSTALLED_DATE = "last_installed_date";
    public static final String SHARED_PREF_INSTALLED_SSID = "config";
    public static final String SHARED_PREF_WIFI_CONFIG = "de.tu_chemnitz.de.wlan.config";

    public static final String SHARED_PREF_NETWORK_SUGGESTIONS_APPROVED = "network_suggestion_approved";

    private static final int REQUEST_LOCATION = 2;

    private static Map<String, List<String>> cookies; // session cookies go here
    private static CookieManager cookieManager = new CookieManager();

    public static String readSharedPreferences(Context context, String prefName, String defaultValue) {
        SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
        return sharedPref.getString(prefName, defaultValue);
    }

    public static void saveSharedPreferences(Context context, String prefName, String prefValue) {
        SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(prefName, prefValue);
        editor.apply();
    }

    public static void DeleteConfigInPreferences(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(SHARED_PREF_WIFI_CONFIG, Context.MODE_PRIVATE);
        if (sharedPrefs.contains("wifi_json")) {
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.remove("wifi_json");
            editor.apply();
        }
    }

    public static void SaveConfigInPreferences(Context context, String prefName, String prefValue){
        SharedPreferences sharedPrefs = context.getSharedPreferences(SHARED_PREF_WIFI_CONFIG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString("wifi_json", prefValue);
        editor.apply();
    }

    public static String ReadConfigPreferences(Context context, String prefName, String defaultValue) {
        SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREF_WIFI_CONFIG, Context.MODE_PRIVATE);
        return sharedPref.getString(prefName, defaultValue);
    }



    @SuppressWarnings("deprecation")
    public static Boolean hasInternet(Context context){
        if(context == null) return false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm!= null) {
            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
                if (capabilities != null) {
                    return capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
                }
                return false;
            } else {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                return (activeNetwork != null && activeNetwork.isConnectedOrConnecting());
            }
        }
        return false;
    }

    public static Boolean hasLockScreenSet(Context context){
        KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        if (km != null) {
            return km.isKeyguardSecure();
        } else
            return false;
    }

    public static void showCheckInternetDialog(final View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle(R.string.onboarding_internet_check_dialog_title);
        builder.setMessage(R.string.onboarding_internet_check_dialog_msg);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            builder.setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    view.getContext().startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });

        } else {

            builder.setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    Helper.enableWifi(view.getContext());
                }
            });
            builder.setNegativeButton(R.string.action_settings, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    view.getContext().startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });
        }
        builder.show();
    }

    public static void showCheckInternetDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.onboarding_internet_check_dialog_title);
        builder.setMessage(R.string.onboarding_internet_check_dialog_msg);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            builder.setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });

        } else {

            builder.setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    Helper.enableWifi(context);
                }
            });
            builder.setNegativeButton(R.string.action_settings, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });
        }
        builder.show();
    }

    public static void showCheckLockScreenDialog(final View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle(R.string.onboarding_lock_screen_check_dialog_title);
        builder.setMessage(R.string.onboarding_lock_screen_check_dialog_msg);
        builder.setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                view.getContext().startActivity(new Intent(DevicePolicyManager.ACTION_SET_NEW_PASSWORD));
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    public static void showLocationPermissionDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.onboarding_location_check_dialog_title);
        builder.setMessage(R.string.onboarding_location_check_dialog_msg);
        builder.setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Helper.requestPermissionDialog(context);
            }
        });
        builder.setNegativeButton(R.string.action_settings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                context.startActivity(intent);
            }
        });
        builder.show();
    }

    public static void setCookies(Map<String, List<String>> map) {
        cookies = map;
        CookieHandler.setDefault(cookieManager);
    }

    public static void resetCookies() {
        cookies = null;
        cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);
    }

    public static void buildHTTPRequest(String url, Response.Listener<String> listener, Response.ErrorListener error, final int method, Boolean isNewConfig, String postBody) {
        RequestQueue queue = Application.getInstance().getRequestQueue();

        if (cookies != null)
            try {
                URI uri = new URI(url);
                cookieManager.put(uri, cookies);
            } catch (Exception e) {
                Log.e("buildHTTPRequest", "Exception", e);
            }

        // new style
        final String requestBody;

        if(postBody !=null) {

            if (isNewConfig) {
                try {
                    requestBody = "device_name=" + URLEncoder.encode(postBody, "utf-8");
                } catch (UnsupportedEncodingException uee) {
                    Log.e("PostDeviceName", "unsupportedEncoding", uee);
                    return;
                }
            } else {
                try {
                    requestBody = "pk=" + URLEncoder.encode(postBody, "utf-8");
                } catch (UnsupportedEncodingException uee) {
                    Log.e("PostPrimaryKey", "unsupportedEncoding", uee);
                    return;
                }
            }
        } else
            requestBody = null;

        StringRequest request;
        request = new StringRequest(
                method,
                url,
                listener,
                error) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null || method != Method.POST ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

        };

        /*
        // old style
        request = new StringRequest(
                Request.Method.POST,
                url,
                listener,
                commonErrorListener) {

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("device_name", device_name);
                params.put("protocol", "" + VERSION);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }

        };*/

        queue.add(request);
    }

    public static String getUsername(String json) {
        try {
            JSONObject jObj = new JSONObject(json);
            if (jObj.has("username"))
                return jObj.getString("username");
        } catch (Exception e) {
        }
        return null;
    }

    public static int getReadyState(String json) {
        /* @return 0 - pending
         * @return <0 - failure
         * @return >0 - success
         */
        try {
            JSONObject jObj = new JSONObject(json);
            /* deploy_state
             * 1 - finished
             * 2 - pending
             * 4 - deploying
             * 5 - failed
             */
            int deploy_state = (jObj.has("deploy_state") ? jObj.getInt("deploy_state") : -1);
            /* state
             * 10 - active
             * 20 - locked
             * 30 - expired
             * 40 - deleted
             */
            int state = (jObj.has("state") ? jObj.getInt("state") : -1);
            if (deploy_state < 0 || state < 0 || deploy_state == 5 || state >= 20)
                return -1;
            if (deploy_state == 1 && state == 10)
                return 1;
            return 0;
        } catch (Exception e) {
        }
        return -1;
    }

    @SuppressWarnings("deprecation")
    public static void enableWifi(Context context) {
        WifiManager wifiMan = (WifiManager)context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (wifiMan != null && !wifiMan.isWifiEnabled())
            wifiMan.setWifiEnabled(true);

        int i = 0;
        if (wifiMan != null) {
            while (!wifiMan.pingSupplicant() && i++ < 100)
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    Log.e("Helper", "enableWifi", e);
                }
        }
    }

    public static boolean checkInstalledConfig(Context context) {
        Log.e("checkInstalledConfig", "Check");
            return Helper.readSharedPreferences(context, Helper.SHARED_PREF_DEVICE_NAME, null) != null;
    }

//    /*This method needs Location Permission for execute*/
//
//    private static boolean isEduroamSaved(Context context) {
//        try {
//            String ssid = "\"eduroam\"";
//            WifiManager wifiMan = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//            if (wifiMan != null) {
//                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(context, "Permission Granted", Toast.LENGTH_SHORT).show();
//                    for (WifiConfiguration conf : wifiMan.getConfiguredNetworks()) {
//                            return ssid.equals(conf.SSID);
//
//                    }
//                }
//            }
//            return false;
//        } catch (Exception e) {
//            Log.e("checkInstalledConfig", "Exception", e);
//            return false;
//        }
//    }

    public static boolean hasLocationPermission(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        } else
            return true;
    }
    public static void requestPermissionDialog(Context context) {
        ActivityCompat.requestPermissions((Activity) context,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_LOCATION);
    }


}
