package com.aadi.tucwlan.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiEnterpriseConfig;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiNetworkSuggestion;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.aadi.tucwlan.R;
import com.aadi.tucwlan.model.ConfigurationResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("deprecation")
public class WifiConfig {

    private static WifiConfiguration lastWifi = null;
    private static WifiEnterpriseConfig newlastWifi;
    private static int lastAddedWifi = -1;

    public static boolean build(String json, Context context) {
        try {
            Log.d("WifiConfig.build()", json);
            JSONObject jObj = new JSONObject(json);
            if (!jObj.has("type") || !jObj.getString("type").equals("wifi_config")) {
                return false;
            } else {

                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                    Log.d("build_for_API_29()", "Build Wifi Config for Android 10");
                    return buildConfig_API_29(jObj, context);
                } else {
                    Log.d("build_for_API_28()", "Build Wifi Config for Android 9 and less");
                    return buildConfig_API_28(jObj, context);
                }
            }

        } catch (Exception e) {
            Log.e("WifiConfig.build()", "Exception", e);
            return false;
        }

    }

    public static boolean apply(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Log.d("apply_Android_10()", "Apply Wifi Config for Android 10");
            return applyConfig_API_29(context);
        }
        else {
            Log.d("apply_Android_9()", "Apply Wifi Config for Android 8 and less");
            return applyConfig_API_22(context);
        }

    }

    private static boolean buildConfig_API_29(JSONObject jObj, Context context) {
        try {

            WifiEnterpriseConfig config = createWifiEnterpriseConfig(
                    jObj.getString("password"),
                    jObj.getString("eapMethod"),
                    (jObj.isNull("phase2") ? null : jObj.getString("phase2")),
                    jObj.getString("identity"),
                    (jObj.isNull("anonymousIdentity") ? null : jObj.getString("anonymousIdentity")),
                    jObj.getString("subjectMatch"),
                    getCertificate(context)); // X509Cert

            newlastWifi = config;

            return true;

        } catch (Exception e) {
            Log.e("buildConfig_API_29", "Exception", e);
            return false;
        }
    }

    public static ConfigurationResponse getConfigurationStatus(Context context) {
       if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
           String cf = Helper.ReadConfigPreferences(context, "wifi_json", "{}");
           try {
               Gson g = new Gson();
               JSONObject jObj = new JSONObject(cf);
               if (!jObj.has("type") || !jObj.getString("type").equals("wifi_config")) {
                   return null;
               } else {
                   WifiEnterpriseConfig config = createWifiEnterpriseConfig(
                           jObj.getString("password"),
                           jObj.getString("eapMethod"),
                           (jObj.isNull("phase2") ? null : jObj.getString("phase2")),
                           jObj.getString("identity"),
                           (jObj.isNull("anonymousIdentity") ? null : jObj.getString("anonymousIdentity")),
                           jObj.getString("subjectMatch"),
                           getCertificate(context)); // X509Cert

                  return extractResponse(config, context);
               }
           } catch (Exception e) {
                   Log.e("getConfigurationStatus", "Exception", e);
                   return null;
           }
       } else {
           Log.d("build_for_API_28()", "Build Wifi Config for Android 9 and less");
           String ssid = "\"eduroam\"";

           WifiManager wifi = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
           if (wifi != null) {
               if (wifi.isWifiEnabled()) {
                   if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                       List<WifiConfiguration> currentConfigs = wifi.getConfiguredNetworks();
                       for (WifiConfiguration currentConfig : currentConfigs) {
                           if (ssid.equals(currentConfig.SSID) && currentConfig.networkId != 0) {
                               return extractResponse(currentConfig, context);
                           }
                       }
                   } else {
                       return null;
                   }
               }
               return null;
           }
           return null;
       }

    }

    private static ConfigurationResponse extractResponse(WifiConfiguration currentConfig, Context context) {
        ConfigurationResponse cr = new ConfigurationResponse();
        String ssid = currentConfig.SSID;

        if (currentConfig.allowedGroupCiphers.toString().contains(",")) ssid+= " with mixed mode";
        else if (currentConfig.allowedGroupCiphers.toString().contains("2")) ssid+= " with TKIP";
        else if (currentConfig.allowedGroupCiphers.toString().contains("3")) ssid+= " with CCMP";
        cr.setSsid(ssid);

        String userId;
        userId = currentConfig.enterpriseConfig.getIdentity();
        if( userId != null && !userId.isEmpty() ) {
            if (userId.contains("@")) {
                userId = userId.substring(0, userId.indexOf('@'));
                if (userId.contains("-")) {
                    userId = userId.substring(0, userId.indexOf('-'));
                }
            }
        } else {
            userId = "unavailable";
        }
        cr.setIdentity(userId);

        String anonymousUserId;
        if (currentConfig.enterpriseConfig.getAnonymousIdentity().length()>0) {
            String anonymousIdentity = currentConfig.enterpriseConfig.getAnonymousIdentity();
            if (!anonymousIdentity.equals(context.getString(R.string.edurom_anonymous_identity))) {
                anonymousUserId = context.getString(R.string.edurom_error_old_configuration, anonymousIdentity);
            } else {
                anonymousUserId = anonymousIdentity;
            }
        } else {
            anonymousUserId= "not_set";
        }
        cr.setAnonymousIdentity(anonymousUserId);

        String message;
        if (currentConfig.enterpriseConfig.getEapMethod()== WifiEnterpriseConfig.Eap.PEAP) message="PEAP with ";
        else if (currentConfig.enterpriseConfig.getEapMethod()== WifiEnterpriseConfig.Eap.PWD) message="PWD with ";
        else if (currentConfig.enterpriseConfig.getEapMethod()== WifiEnterpriseConfig.Eap.TLS) message="TLS";
        else if (currentConfig.enterpriseConfig.getEapMethod()== WifiEnterpriseConfig.Eap.TTLS) message="TTLS with ";
        else {
            message= "Error";
        }
        if (currentConfig.enterpriseConfig.getEapMethod()!= WifiEnterpriseConfig.Eap.TLS)
        {
            if (currentConfig.enterpriseConfig.getPhase2Method()== WifiEnterpriseConfig.Phase2.MSCHAPV2) message+="Phase2: MSCHAPv2";
            else if (currentConfig.enterpriseConfig.getPhase2Method()== WifiEnterpriseConfig.Phase2.GTC) message+="Phase2: GTC";
            else if (currentConfig.enterpriseConfig.getPhase2Method()== WifiEnterpriseConfig.Phase2.PAP) message+="Phase2: PAP";
            else message+="Phase2 Missing";
        }
        cr.setEapMethod(message);

        // WifiManager::getConfiguredNetworks return WifiConfiguration instances with incomplete information
        // There is no right way to verify the CA certificate in a configured WLAN.

        // Transforms WifiEnterpriseConfig instance to string value and check the ca_cert value
        Map<String, String> currentWifiConfigurationValues = new HashMap<>();
        // Parse string representation of enterprise config
        for (String wifiConfigurationLine : currentConfig.enterpriseConfig.toString().split(String.valueOf('\n'))) {
            String wifiConfigurationValueIdentifier = null;
            String wifiConfigurationValue = null;

            // Check if identifier has value and add identifier and maybe value to a map
            String[] wifiConfigurationLineSegments = wifiConfigurationLine.split(String.valueOf(' '));
            if (wifiConfigurationLineSegments.length >= 1) {
                wifiConfigurationValueIdentifier = wifiConfigurationLineSegments[0];
                if (wifiConfigurationLineSegments.length >= 2) {
                    wifiConfigurationValue = wifiConfigurationLineSegments[1];
                }
                currentWifiConfigurationValues.put(wifiConfigurationValueIdentifier, wifiConfigurationValue);
            }
        }
        // If ca_cert is in map/string, then check value.
        String currentWifiConfigurationCaCertValue = currentWifiConfigurationValues.get("ca_cert");
        if (currentWifiConfigurationCaCertValue != null) {
            if (    currentWifiConfigurationCaCertValue.contains("PEAP") && currentWifiConfigurationCaCertValue.contains("eduroam")
                    && currentWifiConfigurationCaCertValue.contains("EAPIEEE8021X") && currentWifiConfigurationCaCertValue.contains("PEAP")) {
                message = "OK";
            } else {
                message = context.getString(R.string.tickline_subtext_ca_cert_certificate_wrong);
            }
        } else {
            message = context.getString(R.string.tickline_subtext_ca_cert_certificate_not_found);
        }
        cr.setCertificateStatus(message);


        String subjectMatch;
        if (Build.VERSION.SDK_INT >= 23)
            subjectMatch = currentConfig.enterpriseConfig.getAltSubjectMatch();
        else
            subjectMatch = currentConfig.enterpriseConfig.getSubjectMatch();

        if (subjectMatch!=null && subjectMatch.length()>0)
            message= "OK";
        else
            message=context.getString(R.string.missing);

        cr.setSubjectMatch(message);
        return cr;
    }

    private static ConfigurationResponse extractResponse(WifiEnterpriseConfig currentConfig, Context context) {
        ConfigurationResponse cr = new ConfigurationResponse();
        cr.setSsid(null);

        String userId;
        userId = currentConfig.getIdentity();
        if( userId != null && !userId.isEmpty() ) {
            if (userId.contains("@")) {
                userId = userId.substring(0, userId.indexOf('@'));
                if (userId.contains("-")) {
                    userId = userId.substring(0, userId.indexOf('-'));
                }
            }
        } else {
            userId = "unavailable";
        }
        cr.setIdentity(userId);

        String anonymousUserId;
        if (currentConfig.getAnonymousIdentity().length()>0) {
            String anonymousIdentity = currentConfig.getAnonymousIdentity();
            if (!anonymousIdentity.equals(context.getString(R.string.edurom_anonymous_identity))) {
                anonymousUserId = context.getString(R.string.edurom_error_old_configuration, anonymousIdentity);
            } else {
                anonymousUserId = anonymousIdentity;
            }
        } else {
            anonymousUserId= "not_set";
        }
        cr.setAnonymousIdentity(anonymousUserId);

        String message;
        if (currentConfig.getEapMethod()== WifiEnterpriseConfig.Eap.PEAP) message="PEAP with ";
        else if (currentConfig.getEapMethod()== WifiEnterpriseConfig.Eap.PWD) message="PWD with ";
        else if (currentConfig.getEapMethod()== WifiEnterpriseConfig.Eap.TLS) message="TLS";
        else if (currentConfig.getEapMethod()== WifiEnterpriseConfig.Eap.TTLS) message="TTLS with ";
        else {
            message= "Error";
        }
        if (currentConfig.getEapMethod()!= WifiEnterpriseConfig.Eap.TLS)
        {
            if (currentConfig.getPhase2Method()== WifiEnterpriseConfig.Phase2.MSCHAPV2) message+="Phase2: MSCHAPv2";
            else if (currentConfig.getPhase2Method()== WifiEnterpriseConfig.Phase2.GTC) message+="Phase2: GTC";
            else if (currentConfig.getPhase2Method()== WifiEnterpriseConfig.Phase2.PAP) message+="Phase2: PAP";
            else message+="Phase2 Missing";
        }
        cr.setEapMethod(message);

        // WifiManager::getConfiguredNetworks return WifiConfiguration instances with incomplete information
        // There is no right way to verify the CA certificate in a configured WLAN.

        // Transforms WifiEnterpriseConfig instance to string value and check the ca_cert value
        Map<String, String> currentWifiConfigurationValues = new HashMap<>();
        // Parse string representation of enterprise config
        for (String wifiConfigurationLine : currentConfig.toString().split(String.valueOf('\n'))) {
            String wifiConfigurationValueIdentifier = null;
            String wifiConfigurationValue = null;

            // Check if identifier has value and add identifier and maybe value to a map
            String[] wifiConfigurationLineSegments = wifiConfigurationLine.split(String.valueOf(' '));
            if (wifiConfigurationLineSegments.length >= 1) {
                wifiConfigurationValueIdentifier = wifiConfigurationLineSegments[0];
                if (wifiConfigurationLineSegments.length >= 2) {
                    wifiConfigurationValue = wifiConfigurationLineSegments[1];
                }
                currentWifiConfigurationValues.put(wifiConfigurationValueIdentifier, wifiConfigurationValue);
            }
        }
        // If ca_cert is in map/string, then check value.
        String currentWifiConfigurationCaCertValue = currentWifiConfigurationValues.get("ca_cert");
        if (currentWifiConfigurationCaCertValue != null) {
            if (    currentWifiConfigurationCaCertValue.contains("PEAP") && currentWifiConfigurationCaCertValue.contains("eduroam")
                    && currentWifiConfigurationCaCertValue.contains("EAPIEEE8021X") && currentWifiConfigurationCaCertValue.contains("PEAP")) {
                message = "OK";
            } else {
                message = context.getString(R.string.tickline_subtext_ca_cert_certificate_wrong);
            }
        } else {
            message = context.getString(R.string.tickline_subtext_ca_cert_certificate_not_found);
        }
        cr.setCertificateStatus(message);


        String subjectMatch;
        if (Build.VERSION.SDK_INT >= 23)
            subjectMatch = currentConfig.getAltSubjectMatch();
        else
            subjectMatch = currentConfig.getSubjectMatch();

        if (subjectMatch!=null && subjectMatch.length()>0)
            message= "OK";
        else
            message=context.getString(R.string.missing);

        cr.setSubjectMatch(message);
        return cr;
    }

    /**
     * Android 10 (API level 29) - Wi-Fi suggestion API for internet connectivity
     * <p>
     * https://developer.android.com/guide/topics/connectivity/wifi-suggest
     */
    @RequiresApi(api = Build.VERSION_CODES.Q)
    private static boolean applyConfig_API_29(Context context) {

        try {
            Log.e("applyConfig_API_29", "Context: " + context);

            if (newlastWifi == null) return false;

            WifiNetworkSuggestion suggestion1 = new WifiNetworkSuggestion.Builder()
                    .setSsid("eduroam")
                    .setWpa2EnterpriseConfig(newlastWifi)
                    .build();


            Log.e("applyConfig_API_29", "Added Suggestion: " + suggestion1);
            final List<WifiNetworkSuggestion> suggestionsList = new ArrayList<WifiNetworkSuggestion>();
            suggestionsList.add(suggestion1);

            final WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            final int status = wifiManager != null ? wifiManager.addNetworkSuggestions(suggestionsList) : 0;

            if (status == WifiManager.STATUS_NETWORK_SUGGESTIONS_SUCCESS) {

//                // Post connection broadcast to one of your suggestions)
//                final IntentFilter intentFilter = new IntentFilter(WifiManager.ACTION_WIFI_NETWORK_SUGGESTION_POST_CONNECTION);
//
//                final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
//                    @Override
//                    public void onReceive(Context context, Intent intent) {
//                        if (!intent.getAction().equals(WifiManager.ACTION_WIFI_NETWORK_SUGGESTION_POST_CONNECTION)) {
//                            return;
//                        }
//                        Helper.saveSharedPreferences(context, Helper.SHARED_PREF_NETWORK_SUGGESTIONS_APPROVED, "true");
//                    }
//                };
//                context.registerReceiver(broadcastReceiver, intentFilter);
                Helper.SaveConfigInPreferences(context, "wifi_json", newlastWifi.toString());

                return true;
            }
            else if (status == WifiManager.STATUS_NETWORK_SUGGESTIONS_ERROR_ADD_DUPLICATE) {
                Log.e("applyConfig_API_29", "Removing Suggestion: " + suggestion1);
                final int status_code = wifiManager.removeNetworkSuggestions(suggestionsList);
                Log.e("applyConfig_API_29", "Status_code: " + status_code);
                if (status_code == WifiManager.STATUS_NETWORK_SUGGESTIONS_SUCCESS) {
                    Log.e("applyConfig_API_29", "Suggestion Removed!");
                    return applyConfig_API_29(context);
                } else {
                    Log.e("applyConfig_API_29", "Error in removing: One or more of the network suggestions removed does not exist in platform's database.");
                    return false;
                }

            } else
                return false;
          /*      return true;
            //TODO: Display feedback for Disallowed cases
            WifiManager.STATUS_NETWORK_SUGGESTIONS_ERROR_APP_DISALLOWED

          */

        } catch (Exception e) {
            Log.e("applyConfig_API_29", "Exception", e);
            return false;
        }

    }

    @SuppressWarnings("deprecation")
    private static boolean buildConfig_API_28(JSONObject jObj, Context context) {

        try {
            WifiConfiguration config = createWifiConfiguration(
                    jObj.getString("ssid"),
                    jObj.getString("password"),
                    jObj.getString("eapMethod"),
                    (jObj.isNull("phase2") ? null : jObj.getString("phase2")),
                    jObj.getString("identity"),
                    (jObj.isNull("anonymousIdentity") ? null : jObj.getString("anonymousIdentity")),
                    jObj.getString("subjectMatch"),
                    getCertificate(context));

            lastWifi = config;

            return true;

        } catch (Exception e) {
            Log.e("buildConfig_API_28", "Exception", e);
            return false;
        }
    }

    private static boolean applyConfig_API_22(Context context) {
        try {
            if (lastWifi == null) return false;

            WifiManager wifiMan = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            Helper.enableWifi(context);

            Log.d("applyConfig_API_22", "addNetwork");
            if (wifiMan != null) {

                lastAddedWifi = wifiMan.addNetwork(lastWifi);

                if (lastAddedWifi == -1) {
                    Log.d("applyConfig_API_22", "updating old eduroam");
                    lastAddedWifi = wifiMan.updateNetwork(lastWifi);
                }

                if (lastAddedWifi == -1) return false;

                Log.d("applyConfig_API_22", "Added network");
                Helper.saveSharedPreferences(context, Helper.SHARED_PREF_INSTALLED_SSID, String.valueOf(lastWifi));
                Helper.saveSharedPreferences(context, Helper.SHARED_PREF_LAST_ADDED_WIFI, String.valueOf(lastAddedWifi));
                return wifiMan.enableNetwork(lastAddedWifi, false) && wifiMan.saveConfiguration();
            }
            return false;
        } catch (Exception e) {
            Log.e("applyConfig_API_22", "Exception", e);
            return false;
        }
    }

    /**
     * Create a {@link WifiEnterpriseConfig} for an Android 10 (API 29) for an EAP secured network
     *
     * @param password          The password
     * @param eapMethod         The EAP method
     * @param phase2            The phase 2 method or null
     * @param identity          The identity or null
     * @param anonymousIdentity The anonymous identity or null
     *                          // following 2 are marked with @hide so it is not possible to access them from outside the android API
     *                          //@param caCert The CA certificate or null
     *                          //@param clientCert The client certificate or null
     * @return The {@link WifiEnterpriseConfig}
     */
    @SuppressLint("NewApi")
    private static WifiEnterpriseConfig createWifiEnterpriseConfig(String password, String eapMethod, String phase2, String identity,
                                                                   String anonymousIdentity, String subjectMatch, X509Certificate[] certs) {


        Integer _phase2;
        // Set defaults
        _phase2 = (phase2 == null) ? WifiEnterpriseConfig.Phase2.NONE : getPhase2(phase2);

        if (identity == null) identity = "";
        if (anonymousIdentity == null) anonymousIdentity = "";

        WifiEnterpriseConfig config = new WifiEnterpriseConfig();

        config.setPassword(password);
        config.setEapMethod(getEapMethod(eapMethod));
        config.setPhase2Method(_phase2);
        config.setIdentity(identity);
        config.setAnonymousIdentity(anonymousIdentity);
        config.setAltSubjectMatch("DNS:" + subjectMatch);

        for (X509Certificate cert : certs) {
            // test if certificate is a root CA
            if (cert.getIssuerX500Principal().equals(cert.getSubjectX500Principal())) {
                Log.d("createEapConfig", "Adding CA certificate " + cert.getSubjectX500Principal().getName());
                config.setCaCertificate(cert);
            }
        }
        Log.d("createEapConfig", config.getCaCertificate().getSubjectX500Principal().toString());
        return config;
    }


    /**
     * Create a {@link WifiConfiguration} for an EAP secured network
     *
     * @param ssid              The SSID of the wifi network
     * @param password          The password
     * @param eapMethod         The EAP method
     * @param phase2            The phase 2 method or null
     * @param identity          The identity or null
     * @param anonymousIdentity The anonymous identity or null
     *                          // following 2 are marked with @hide so it is not possible to access them from outside the android API
     *                          //@param caCert The CA certificate or null
     *                          //@param clientCert The client certificate or null
     * @return The {@link WifiConfiguration}
     */
    @SuppressWarnings("deprecation")
    private static WifiConfiguration createWifiConfiguration(String ssid, String password, String eapMethod, String phase2, String identity,
                                                             String anonymousIdentity, String subjectMatch, X509Certificate[] certs) {
        WifiConfiguration config = new WifiConfiguration();
        config.SSID = "\"" + ssid + "\"";

        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.IEEE8021X);

        Integer _phase2;
        // Set defaults
        if (phase2 == null) _phase2 = WifiEnterpriseConfig.Phase2.NONE;
        else _phase2 = getPhase2(phase2);
        if (identity == null) identity = "";
        if (anonymousIdentity == null) anonymousIdentity = "";

        config.enterpriseConfig.setPassword(password);
        config.enterpriseConfig.setEapMethod(getEapMethod(eapMethod));
        config.enterpriseConfig.setPhase2Method(_phase2);
        config.enterpriseConfig.setIdentity(identity);
        config.enterpriseConfig.setAnonymousIdentity(anonymousIdentity);
        if (Build.VERSION.SDK_INT >= 23) {
            config.enterpriseConfig.setAltSubjectMatch("DNS:" + subjectMatch);
            config.enterpriseConfig.setDomainSuffixMatch("tu-chemnitz.de");
        } else
            config.enterpriseConfig.setSubjectMatch(subjectMatch);
        // API >= 24 would allow us to set all certificates at once, but let's try to
        // set each certificate individually to possibly work around issue #10
        for (X509Certificate cert : certs) {
            // test if certificate is a root CA
            if (cert.getIssuerX500Principal().equals(cert.getSubjectX500Principal())) {
                Log.d("createEapConfig", "Adding CA certificate " + cert.getSubjectX500Principal().getName());
                config.enterpriseConfig.setCaCertificate(cert);
            }
        }
        //config.enterpriseConfig.setCaCertificateAlias(caCert);
        //config.enterpriseConfig.setClientCertificateAlias(clientCert);
        Log.d("createEapConfig", config.enterpriseConfig.getCaCertificate().getSubjectX500Principal().toString());
        return config;
    }


    private static X509Certificate[] getCertificate(Context context) {

        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X.509");
        } catch (CertificateException e) {
            e.printStackTrace();
        }

        X509Certificate[] x509s = new X509Certificate[1];
        AssetManager assetManager = context.getAssets();
        InputStream eduromCertificateStream = null;

        try {
            eduromCertificateStream = assetManager.open("ca.pem");
            X509Certificate eduromCertificate = null;
            if (cf != null) {
                eduromCertificate = (X509Certificate) cf.generateCertificate(eduromCertificateStream);
            }
            x509s[0] = eduromCertificate;
        } catch (IOException exception) {
            Log.e("buildWifiConfig", "Can't open certificate", exception);
        } catch (CertificateException exception) {
            Log.e("buildWifiConfig", "Can't read or process certificate", exception);
        } catch (Throwable throwable) {
            Log.e("buildWifiConfig", "Unexpected error", throwable);
        } finally {
            if (eduromCertificateStream != null) {
                try {
                    eduromCertificateStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return x509s;
    }

    public static String getCheckURL(String json) {
        try {
            JSONObject jObj = new JSONObject(json);
            if (jObj.has("deploy_status"))
                return jObj.getString("deploy_status");
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * Get the EAP method from a string.
     *
     * @throws IllegalArgumentException if the string is not a supported EAP method.
     */
    private static int getEapMethod(String eapMethod) {
        if ("TLS".equalsIgnoreCase(eapMethod)) {
            return WifiEnterpriseConfig.Eap.TLS;
        }
        if ("TTLS".equalsIgnoreCase(eapMethod)) {
            return WifiEnterpriseConfig.Eap.TTLS;
        }
        if ("PEAP".equalsIgnoreCase(eapMethod)) {
            return WifiEnterpriseConfig.Eap.PEAP;
        }
        throw new IllegalArgumentException("EAP method must be one of TLS, TTLS, or PEAP");
    }


    /**
     * Get the phase 2 method from a string.
     *
     * @throws IllegalArgumentException if the string is not a supported phase 2 method.
     */
    private static int getPhase2(String phase2) {
        if ("PAP".equalsIgnoreCase(phase2)) {
            return WifiEnterpriseConfig.Phase2.PAP;
        }
        if ("MSCHAP".equalsIgnoreCase(phase2)) {
            return WifiEnterpriseConfig.Phase2.MSCHAP;
        }
        if ("MSCHAPV2".equalsIgnoreCase(phase2)) {
            return WifiEnterpriseConfig.Phase2.MSCHAPV2;
        }
        if ("GTC".equalsIgnoreCase(phase2)) {
            return WifiEnterpriseConfig.Phase2.GTC;
        }
        throw new IllegalArgumentException("Phase2 must be one of PAP, MSCHAP, MSCHAPV2, or GTC");
    }

}
