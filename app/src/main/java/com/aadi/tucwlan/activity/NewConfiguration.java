package com.aadi.tucwlan.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;

import com.aadi.tucwlan.R;
import com.aadi.tucwlan.utils.Helper;

public class NewConfiguration extends AppCompatActivity implements View.OnClickListener {
    private AutoCompleteTextView inputDeviceName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_configuration);

        ((TextView) findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.new_configuration));
        ((AppCompatImageButton) findViewById(R.id.toolbar_left_btn)).setOnClickListener(this);
        ((AppCompatImageButton) findViewById(R.id.toolbar_right_btn)).setOnClickListener(this);
        ((AppCompatButton) findViewById(R.id.button_done)).setOnClickListener(this);

        inputDeviceName = (AutoCompleteTextView) findViewById(R.id.input_device_name);
        inputDeviceName.setText(getApplicationContext().getResources().getString(R.string.input_device_name, Build.MANUFACTURER, Build.MODEL, Build.VERSION.SDK_INT));

    }

    private void onDoneButtonClick() {
        if(Helper.hasInternet(this)) {
            if (!(inputDeviceName.getText().toString().equals(""))) {
                Intent intent = new Intent(NewConfiguration.this, InstallingConfiguration.class);
                intent.putExtra("DEVICE_NAME", inputDeviceName.getText().toString());
                intent.putExtra("NEW_CONFIGURATION", true);
                startActivity(intent);
                finish();
            } else
                showEmptyInputDialog();
        } else
            Helper.showCheckInternetDialog(NewConfiguration.this);
    }

    private void showEmptyInputDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(NewConfiguration.this);
        builder.setTitle(R.string.new_configuration_check_dialog_title);
        builder.setMessage(R.string.new_configuration_check_dialog_msg);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.toolbar_left_btn) {
            onBackPressed();
        } else {
            onDoneButtonClick();
        }
    }
}
