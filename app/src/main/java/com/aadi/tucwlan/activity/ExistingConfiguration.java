package com.aadi.tucwlan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aadi.tucwlan.R;
import com.aadi.tucwlan.adapter.ConfigurationAdapter;
import com.aadi.tucwlan.model.Configuration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ExistingConfiguration extends AppCompatActivity {

    private final String TAG = ExistingConfiguration.class.getSimpleName();

    private LinearLayoutManager linearLayoutManager;
    public static RecyclerView.Adapter adapter;
    private List<Configuration> mConfiguration;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existing_configurations);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String username = getIntent().getStringExtra("USERNAME");
        String response = getIntent().getStringExtra("RESPONSE");

        if (response != null) {

            ((TextView) findViewById(R.id.logged_in_as)).setText(getResources().getString(R.string.message_logged_in_as, username));

            mRecyclerView = findViewById(R.id.recyclerView);

            mConfiguration = new ArrayList<>();
            adapter = new ConfigurationAdapter(getApplicationContext(), mConfiguration);

            linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(RecyclerView.VERTICAL);

            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(linearLayoutManager);
            mRecyclerView.setAdapter(adapter);

            try {

                JSONObject obj = new JSONObject(response);
                JSONArray arr = obj.getJSONArray("configurations");

                for (int i = 0; i < arr.length(); i++) {
                    try {
                        JSONObject jobj = arr.getJSONObject(i);

                        Configuration config = new Configuration();
                        config.setDeviceName(jobj.getString("device_name"));
                        config.setLastUsed(jobj.getString("last_used"));
                        config.setResetUri(jobj.getString("reset_uri"));
                        config.setDeployState(jobj.getInt("deploy_state"));
                        config.setState(jobj.getInt("state"));
                        config.setPk(jobj.getInt("pk"));

                        /*
                        *
                        *   For debugging only.
                        *
                        *   Log.d(TAG, "Device Name: " + jobj.getString("device_name"));
                        *   Log.d(TAG, "Last Used: " + jobj.getString("last_used"));
                        *   Log.d(TAG, "Reset Uri: " + jobj.getString("reset_uri"));
                        *   Log.d(TAG, "Deploy State: " + jobj.getInt("deploy_state"));
                        *   Log.d(TAG, "State: " + jobj.getInt("state"));
                        *
                        */

                        mConfiguration.add(config);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(TAG, getResources().getString(R.string.msg_parsing_error, e));
                        Toast.makeText(ExistingConfiguration.this, getResources().getString(R.string.msg_general_error), Toast.LENGTH_SHORT).show();
                        finish();

                    }

                }
                Log.d(TAG, "No. of configuration added" + arr.length());
                adapter.notifyDataSetChanged();

            } catch (JSONException e) {
                Log.e(TAG, getResources().getString(R.string.msg_parsing_error, e));
                Toast.makeText(ExistingConfiguration.this, getResources().getString(R.string.msg_general_error), Toast.LENGTH_SHORT).show();
                finish();
            }

            ((TextView) findViewById(R.id.view_more)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (adapter != null)
                        startActivity(new Intent(ExistingConfiguration.this, AllConfiguration.class));
                    else {
                        Toast.makeText(ExistingConfiguration.this, getResources().getString(R.string.msg_general_error), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });

            ((AppCompatButton) findViewById(R.id.button_title)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(ExistingConfiguration.this, NewConfiguration.class));
                    finish();
                }
            });

        } else {
            startActivity(new Intent(ExistingConfiguration.this, LoginActivity.class));
            finish();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_existing_configurations, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.item_01) {
            startActivity(new Intent(ExistingConfiguration.this, LoginActivity.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
