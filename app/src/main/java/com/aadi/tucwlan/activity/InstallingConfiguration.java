package com.aadi.tucwlan.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.aadi.tucwlan.R;
import com.aadi.tucwlan.fragments.FeedbackBottomSheet;
import com.aadi.tucwlan.utils.Helper;
import com.aadi.tucwlan.utils.WifiConfig;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.text.DateFormat;
import java.util.Date;


public class InstallingConfiguration extends AppCompatActivity {

    private final String TAG = InstallingConfiguration.class.getSimpleName();

    private LottieAnimationView animationView;
    private TextView titleView;
    private TextView subtitleView;
    private TextView caption_view;
    private Button button_1;
    private Button button_2;

    private Handler mHandler = new Handler();

    //Long Polling TimeOut in Millisecond
    private int mInterval = 1000;

    private String inputDeviceName;
    private TextView datetime_view;
    private Date installed_date;
    private boolean isNewConfiguration;
    private boolean isFeedbackFragmentOpen = false;

    private NetworkChangeReceiver networkChangeReceiver = null;
    private String checkURL = null;

    final private int STATUS_CONFIGURATION_NO_INTERNET = -2;
    final private int STATUS_CONFIGURATION_LOADING = -1;
    final private int STATUS_CONFIGURATION_SUCCESS = 0;
    final private int STATUS_CONFIGURATION_ERROR_FAILED_TO_APPLY = 1;
    final private int STATUS_CONFIGURATION_ERROR_FAILED_TO_BUILD = 2;
    final private int STATUS_CONFIGURATION_ERROR_DEPLOYMENT = 3;
    final private int STATUS_CONFIGURATION_ERROR_SESSION_EXPIRE = 4;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initializing Layout
        initLayout();

        final String targetURL;
        final String requestBody;

        //Check for New Configuration or using from old Configurations
        isNewConfiguration = getIntent().getBooleanExtra("NEW_CONFIGURATION", true);
        inputDeviceName = getIntent().getStringExtra("DEVICE_NAME");
        Log.v(TAG, "Is a New Config: " + isNewConfiguration);

        if (isNewConfiguration) {
            inputDeviceName = getIntent().getStringExtra("DEVICE_NAME");
            targetURL = Helper.API_URL;
            requestBody = inputDeviceName;
        } else {
            inputDeviceName = getIntent().getStringExtra("DEVICE_NAME");
            int pk = getIntent().getIntExtra("PRIMARY_KEY", -1);
            targetURL = getIntent().getStringExtra("RESET_URI");
            requestBody = String.valueOf(pk);
        }

            Helper.buildHTTPRequest(targetURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "Response Received: " + response);
                    if (!response.startsWith("{")) {

                        //If user is not in session.. Redirecting to LoginActivity.
                        Intent intent = new Intent(InstallingConfiguration.this, LoginActivity.class);
                        intent.putExtra("INSTALL_FLAG", true);
                        intent.putExtra("DEVICE_NAME", inputDeviceName);
                        intent.putExtra("NEW_CONFIGURATION", isNewConfiguration);
                        Log.d(TAG, (getResources().getString(R.string.msg_installing_configuration_on_device)));
                        startActivity(intent);
                        InstallingConfiguration.this.finish();

                    } else {
                        titleView.setText(getResources().getString(R.string.msg_installing_configuration_on_device));
                        Log.d(TAG, "Configuration Received!");
                        Log.d(TAG, "WifiConfig.build");

                        if (WifiConfig.build(response, InstallingConfiguration.this)) {
                            Log.d(TAG, "WifiConfig.applyWifiConfig");
                            if (WifiConfig.apply(InstallingConfiguration.this)) {
                                Log.d(TAG, "WifiConfig.getCheckURL");
                                checkURL = WifiConfig.getCheckURL(response);
                                startLongPollingCheck();
                            } else
                                updateUI(STATUS_CONFIGURATION_ERROR_FAILED_TO_APPLY);
                        } else
                            updateUI(STATUS_CONFIGURATION_ERROR_FAILED_TO_BUILD);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "getConfig", error);

                    if (error instanceof NetworkError || error instanceof ServerError || error instanceof TimeoutError) {
                        updateUI(STATUS_CONFIGURATION_NO_INTERNET);
                    } else
                        updateUI(STATUS_CONFIGURATION_ERROR_FAILED_TO_APPLY);
                }
            }, Request.Method.POST, isNewConfiguration, requestBody);

    }

    private void longPollingCheck() {
        Log.d(TAG , "LongPollingCheck()");

        if (checkURL != null) {
            Helper.buildHTTPRequest(checkURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    int state = Helper.getReadyState(response);
                    Log.d(TAG , "Long Polling Response: " + response);
                    if (state > 0) {
                        stopLongPollingCheck();
                        installed_date = new Date();
                        Helper.saveSharedPreferences(InstallingConfiguration.this, Helper.SHARED_PREF_WIFI_CONFIG, response);
                        Helper.saveSharedPreferences(InstallingConfiguration.this, Helper.SHARED_PREF_DEVICE_NAME, inputDeviceName);
                        Helper.saveSharedPreferences(InstallingConfiguration.this, Helper.SHARED_PREF_INSTALLED_DATE, DateFormat.getDateTimeInstance().format(installed_date));
                        updateUI(STATUS_CONFIGURATION_SUCCESS);

                    } else if (state == -1) {
                        stopLongPollingCheck();
                        updateUI(STATUS_CONFIGURATION_ERROR_DEPLOYMENT);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "getInstallStatus - checkURL", error);
                    stopLongPollingCheck();
                    if (error.networkResponse != null && error.networkResponse.statusCode == 403) {
                        // Forbidden -> our auth cookies are invalid, somehow?
                        // possibly one user got checkURL -> other user tries to sign in who is not permitted to check this resource
                        updateUI(STATUS_CONFIGURATION_ERROR_SESSION_EXPIRE);
                    }
                    else if (error instanceof NetworkError || error instanceof ServerError || error instanceof TimeoutError) {
                        updateUI(STATUS_CONFIGURATION_NO_INTERNET);
                    }
                    else
                        updateUI(STATUS_CONFIGURATION_ERROR_DEPLOYMENT);

                }
            }, Request.Method.GET, false, null);
        } else
            updateUI(STATUS_CONFIGURATION_ERROR_DEPLOYMENT);

    }

    private void initLayout() {
        animationView = (LottieAnimationView) findViewById(R.id.animation_view);
        titleView = (TextView) findViewById(R.id.title_view);
        subtitleView = (TextView) findViewById(R.id.subtitle_view);
        caption_view = (TextView) findViewById(R.id.caption_view);
        datetime_view = (TextView) findViewById(R.id.datetime_view);
        button_1 = (Button) findViewById(R.id.button_1);
        button_2 = (Button) findViewById(R.id.button_2);

        titleView.setText(getResources().getString(R.string.msg_logging_in));
        caption_view.setVisibility(View.VISIBLE);
        caption_view.setText(getResources().getString(R.string.msg_installing_subtitle));
        animationView.setAnimation("loading_animation.json");
        animationView.playAnimation();
        animationView.setRepeatCount(LottieDrawable.INFINITE);

        button_1.setVisibility(View.GONE);
        button_2.setVisibility(View.GONE);
        subtitleView.setVisibility(View.GONE);
        datetime_view.setVisibility(View.GONE);
    }

    private void updateUI(int status) {

        if(status == STATUS_CONFIGURATION_SUCCESS){
            animationView.setImageDrawable(ContextCompat.getDrawable(InstallingConfiguration.this, R.drawable.ic_check_circle));
            titleView.setText(getResources().getString(R.string.eduroam_configured));
            caption_view.setText(getResources().getText(R.string.installed_device_configuration));
            subtitleView.setText(getResources().getString(R.string.device_name, inputDeviceName));
            datetime_view.setText(getResources().getString(R.string.msg_installed_on, DateFormat.getDateTimeInstance().format(installed_date)));
            button_2.setText(getResources().getString(R.string.exit));

            animationView.setVisibility(View.VISIBLE);
            titleView.setVisibility(View.VISIBLE);
            subtitleView.setVisibility(View.VISIBLE);
            caption_view.setVisibility(View.VISIBLE);
            datetime_view.setVisibility(View.INVISIBLE);
            button_2.setVisibility(View.VISIBLE);

            button_2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityCompat.finishAffinity(InstallingConfiguration.this);
                }
            });

            if (Build.VERSION.SDK_INT ==  Build.VERSION_CODES.Q) {
                final Boolean isApproved = Boolean.valueOf(Helper.readSharedPreferences(InstallingConfiguration.this, Helper.SHARED_PREF_NETWORK_SUGGESTIONS_APPROVED, "false"));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(!isFeedbackFragmentOpen && !isApproved) {
                            FeedbackBottomSheet fragment = new FeedbackBottomSheet();
                            fragment.show(getSupportFragmentManager(), "dialog");
                            fragment.setCancelable(false);
                            isFeedbackFragmentOpen = true;
                        }
                    }
                }, 500);
            }

        }

        if(status == STATUS_CONFIGURATION_ERROR_FAILED_TO_APPLY){
            titleView.setText(getResources().getString(R.string.error));
            subtitleView.setVisibility(View.VISIBLE);
            subtitleView.setText(getResources().getString(R.string.msg_failed_to_apply_config));
            caption_view.setText(getResources().getString(R.string.installation_failed));
            animationView.setImageDrawable(ContextCompat.getDrawable(InstallingConfiguration.this, R.drawable.ic_error));

        }

        if(status == STATUS_CONFIGURATION_ERROR_FAILED_TO_BUILD){
            titleView.setText(getResources().getString(R.string.error));
            subtitleView.setVisibility(View.VISIBLE);
            subtitleView.setText(getResources().getString(R.string.msg_failed_to_build_config));
            caption_view.setText(getResources().getString(R.string.installation_failed));
            animationView.setImageDrawable(ContextCompat.getDrawable(InstallingConfiguration.this, R.drawable.ic_error));

        }

        if(status == STATUS_CONFIGURATION_LOADING){
            titleView.setText(getResources().getString(R.string.msg_deploying_configuration_on_device));
            animationView.setAnimation("loading_animation.json");
            animationView.playAnimation();
            animationView.setRepeatCount(LottieDrawable.INFINITE);
        }

        if(status == STATUS_CONFIGURATION_ERROR_DEPLOYMENT) {
            animationView.setImageDrawable(ContextCompat.getDrawable(InstallingConfiguration.this, R.drawable.ic_cancel));
            titleView.setText(getResources().getString(R.string.installation_failed));
        }

        if(status == STATUS_CONFIGURATION_ERROR_SESSION_EXPIRE) {
            Helper.resetCookies();
            Log.d(TAG , "resetCookies()");
            titleView.setText(getResources().getString(R.string.login_error));
            caption_view.setText(getResources().getString(R.string.login_error_subtitle));
            animationView.setImageDrawable(ContextCompat.getDrawable(InstallingConfiguration.this, R.drawable.ic_error));
        }

        if(status == STATUS_CONFIGURATION_NO_INTERNET) {

            animationView.setAnimation("no_internet.json");
            animationView.playAnimation();
            animationView.setRepeatCount(LottieDrawable.INFINITE);
            titleView.setText(getString(R.string.no_internet_connection));
            caption_view.setText(R.string.caption_no_internet);
        }

    }

    Runnable mLongPollingCheck = new Runnable() {
        @Override
        public void run() {
            try {
                updateUI(STATUS_CONFIGURATION_LOADING);
                longPollingCheck();
            }
            finally {
                mHandler.postDelayed(mLongPollingCheck, mInterval);
            }
        }
    };

    void stopLongPollingCheck(){
        mHandler.removeCallbacks(mLongPollingCheck);
    }

    void startLongPollingCheck(){
        mLongPollingCheck.run();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLongPollingCheck();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter networkChangeReceiverIntentFilter = new IntentFilter();
        networkChangeReceiverIntentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        networkChangeReceiverIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        this.networkChangeReceiver = new NetworkChangeReceiver();
        Log.d("networkChange_broadcast", "register receiver");
        this.registerReceiver(this.networkChangeReceiver, networkChangeReceiverIntentFilter);

    }

    private class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d("networkChange_broadcast", "Get action: " + intent.getAction());
                updateLayout();
            } catch (Exception exception) {
                Log.d("networkChange_broadcast", "Error while recompute ticklist", exception);
            }
        }
    }

    private void updateLayout() {
        if (Helper.hasInternet(this)) {
            if(checkURL!=null) {
                startLongPollingCheck();
            }
        } else {
            updateUI(STATUS_CONFIGURATION_NO_INTERNET);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.unregisterReceiver(this.networkChangeReceiver);
    }
}
