package com.aadi.tucwlan.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aadi.tucwlan.R;
import com.aadi.tucwlan.utils.Helper;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private final String TAG = LoginActivity.class.getSimpleName();

    private static String username = null;
    private boolean sawLogin = false;
    
    private WebView browser;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ScrollView loading_layout;
    private LottieAnimationView animation_view;
    private TextView title_view;
    private TextView subtitle_view;
    private LinearLayout webview_layout;
    private NetworkChangeReceiver networkChangeReceiver = null;
    private boolean install_flag;
    private String inputDeviceName;
    private boolean isNewConfiguration;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        install_flag = getIntent().getBooleanExtra("INSTALL_FLAG" ,false);
        inputDeviceName = getIntent().getStringExtra("DEVICE_NAME");
        isNewConfiguration = getIntent().getBooleanExtra("NEW_CONFIGURATION", false);

        loading_layout = ((ScrollView) findViewById(R.id.loading_layout));
        animation_view = (LottieAnimationView) findViewById(R.id.animation_view);
        title_view = (TextView) findViewById(R.id.title_view);
        subtitle_view = (TextView) findViewById(R.id.subtitle_view);
        webview_layout = (LinearLayout)findViewById(R.id.webView_layout);
        browser = (WebView) findViewById(R.id.webView);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshlayout);
        browserSettings();

    }

    private void displayNoNetworkLayout() {
        loading_layout.setVisibility(View.VISIBLE);
        animation_view.setAnimation("no_internet.json");
        animation_view.playAnimation();
        animation_view.setRepeatCount(LottieDrawable.INFINITE);
        title_view.setText(getString(R.string.no_internet_connection));
    }

    private void displayLoadingLayout() {
        loading_layout.setVisibility(View.VISIBLE);
        animation_view.setAnimation("loading_animation.json");
        animation_view.playAnimation();
        title_view.setText(getString(R.string.loading));
    }

    private void displayWebViewLayout() {

        loading_layout.setVisibility(View.GONE);
        webview_layout.setVisibility(View.VISIBLE);
        getConfig();

    }

    private void getConfig() {
        Helper.buildHTTPRequest(Helper.API_USER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                username = Helper.getUsername(response);
                if (username != null) {
                    browser.setVisibility(View.INVISIBLE);

                    Helper.buildHTTPRequest(Helper.API_URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String res) {
                            Log.d(TAG , "Response" + res);

                            if(install_flag)
                            {
                                Intent intent = new Intent(LoginActivity.this, InstallingConfiguration.class);
                                intent.putExtra("DEVICE_NAME", inputDeviceName);
                                intent.putExtra("NEW_CONFIGURATION" , isNewConfiguration);
                                startActivity(intent);
                                finish();

                            } else {

                                try {

                                    JSONObject obj = new JSONObject(res);
                                    JSONArray arr = obj.getJSONArray("configurations");

                                    if (arr.length() == 0) {
                                        Intent intent = new Intent(LoginActivity.this, NoConfiguration.class);
                                        intent.putExtra("USERNAME", username);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Intent intent = new Intent(LoginActivity.this, ExistingConfiguration.class);
                                        intent.putExtra("USERNAME", username);
                                        intent.putExtra("RESPONSE", res);
                                        startActivity(intent);
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Intent intent = new Intent(LoginActivity.this, NoConfiguration.class);
                                    intent.putExtra("USERNAME", username);
                                    startActivity(intent);
                                    finish();
                                }

                                Helper.saveSharedPreferences(LoginActivity.this, Helper.SHARED_PREF_NEW_USER, "false");
                            }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
                                    Toast.makeText(LoginActivity.this, "JSON Error: " + volleyError, Toast.LENGTH_LONG).show();
                                    Log.d(TAG,"JSON Error: " + volleyError);
                                }
                            }, Request.Method.GET, false, null);
                }

                else {
                    browser.loadUrl(Helper.API_URL);
                    Log.d(TAG , response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "getConfig", error);
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        }, Request.Method.GET, false, null);
    }

    private void browserSettings() {
        // set javascript and zoom and some other settings
        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setBuiltInZoomControls(true);
        browser.getSettings().setDisplayZoomControls(false);
        browser.getSettings().setAppCacheEnabled(true);
        browser.getSettings().setDatabaseEnabled(true);
        browser.getSettings().setDomStorageEnabled(true);
        browser.getSettings().setUseWideViewPort(true);
        browser.getSettings().setLoadWithOverviewMode(true);

        // enable all plugins (flash)
        browser.getSettings().setPluginState(WebSettings.PluginState.ON);

        browser.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                checkAPIUrl(url);
            }

            @SuppressWarnings("deprecation")
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Uri uri = Uri.parse(url);
                return checkUrl(uri);
            }

            @TargetApi(Build.VERSION_CODES.N)
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return checkUrl(request.getUrl());
            }

            private void checkAPIUrl(String str) {
                if (str.contains(Helper.API_URL)) {
                    Log.d("checkUrl", "in API check " + str);
                    //WebView wv = (WebView)findViewById(R.id.webView_login);
                    CookieSyncManager.getInstance().sync();
                    CookieManager cm = CookieManager.getInstance();
                    Log.d("checkUrl", "got cookie manager");
                    String c = null;
                    if (cm.hasCookies()) {
                        Log.d("checkAPIUrl", "has cookies");
                        c = cm.getCookie(Helper.API_URL);
                        Log.d("checkUrl", "got cookie");
                    }
                    if (c == null) c = "";

                    Log.d("checkUrl", "Cookies: " + c);

                    Map<String, List<String>> map = new HashMap<>();
                    map.put("set-cookie", Arrays.asList(c.split(";")));

                    Helper.setCookies(map);

                    getConfig();

                } else {
                    Log.d("checkUrl", "not in API check "+ str);
                }
            }

            private boolean checkUrl(Uri url) {
                String str = url.toString();
                Log.d("checkUrl", str);

                // don't check cookies here! this breaks below android 5
                //checkAPIUrl(str);

                // don't show API (but redirect to login)
                if (str.contains(Helper.API_URL) && !sawLogin) {
                    sawLogin = true;
                    return false;
                }

                // following ensures user does not leave the shibboleth login
                if (str.contains(".js") ||
                        str.contains(".css") ||
                        str.contains(".png") ||
                        str.contains(".svg") ||
                        str.contains(".gif") ||
                        str.contains(".ico") ||
                        str.contains("hibboleth") ||
                        str.contains("krb") ||
                        str.contains("sso") ||
                        str.contains("?") ||
                        str.contains(".woff2")) // font for captcha and stuff
                    return false;

                Log.d("checkUrl", "Going to stop loading...");
                return true;
            }
        });

        browser.setWebChromeClient(new WebChromeClient()
        {

            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (mSwipeRefreshLayout.isRefreshing()) {
                    if (progress == 100) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                } else if (progress < 100) {
                    //If we do not hide the navigation, show refreshing
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter networkChangeReceiverIntentFilter = new IntentFilter();
        networkChangeReceiverIntentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        networkChangeReceiverIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        this.networkChangeReceiver = new NetworkChangeReceiver();
        Log.d("networkChange_broadcast", "register receiver");
        this.registerReceiver(this.networkChangeReceiver, networkChangeReceiverIntentFilter);

    }

    private class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d("networkChange_broadcast", "Get action: " + intent.getAction());
                updateLayout();
            } catch (Exception exception) {
                Log.d("networkChange_broadcast", "Error while recompute ticklist", exception);
            }
        }
    }

    private void updateLayout() {
        if (Helper.hasInternet(this)) {
            displayWebViewLayout();
        } else {
            displayNoNetworkLayout();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.unregisterReceiver(this.networkChangeReceiver);
    }
}
