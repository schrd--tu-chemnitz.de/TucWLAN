package com.aadi.tucwlan;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.aadi.tucwlan.activity.About;
import com.aadi.tucwlan.activity.DiagnosticActivity;
import com.aadi.tucwlan.activity.LoginActivity;
import com.aadi.tucwlan.activity.NewConfiguration;
import com.aadi.tucwlan.utils.Helper;
import com.airbnb.lottie.LottieAnimationView;

public class MainActivity extends AppCompatActivity {

    private LottieAnimationView animationView;
    private TextView titleView;
    private TextView subtitleView;
    private TextView caption_View;
    private Button button_1;
    private Button button_2;
    private String device_name;
    private String installed_date;
    private TextView datetime_View;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setLayoutDesign();

        updateUI(Helper.checkInstalledConfig(MainActivity.this));
    }

    private void setLayoutDesign() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        animationView = (LottieAnimationView) findViewById(R.id.animation_view);
        titleView = (TextView) findViewById(R.id.title_view);
        subtitleView = (TextView) findViewById(R.id.subtitle_view);
        caption_View = (TextView) findViewById(R.id.caption_view);
        button_1 = (Button) findViewById(R.id.button_1);
        button_2 = (Button) findViewById(R.id.button_2);
        datetime_View = (TextView) findViewById(R.id.datetime_view);

        animationView.setVisibility(View.VISIBLE);
        titleView.setVisibility(View.VISIBLE);
        subtitleView.setVisibility(View.VISIBLE);
        caption_View.setVisibility(View.VISIBLE);
        datetime_View.setVisibility(View.VISIBLE);
        button_1.setVisibility(View.VISIBLE);
        button_2.setVisibility(View.VISIBLE);

        button_1.setText(getResources().getString(R.string.test_connection));
        button_2.setText(getResources().getString(R.string.new_configuration));

        button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, DiagnosticActivity.class));
            }
        });


        button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NewConfiguration.class));
            }
        });

    }

    private void updateUI(Boolean isConfigured) {
        if(isConfigured){
            device_name = Helper.readSharedPreferences(this, Helper.SHARED_PREF_DEVICE_NAME, null);
            installed_date = Helper.readSharedPreferences(this, Helper.SHARED_PREF_INSTALLED_DATE, null);

            animationView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check_circle));
            titleView.setText(getResources().getString(R.string.success));
            subtitleView.setText(getResources().getString(R.string.device_name, device_name));
            datetime_View.setText(getResources().getString(R.string.msg_installed_on, installed_date));
            caption_View.setText(getResources().getText(R.string.installed_device_configuration));

        } else {
            animationView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_error));
            titleView.setText(getResources().getString(R.string.error));
            subtitleView.setText(getResources().getString(R.string.msg_eduroam_not_enabled));
            datetime_View.setVisibility(View.GONE);
            caption_View.setText(getResources().getText(R.string.caption_eduroam_not_enabled));
            button_1.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.item_01) {
            Intent intent = new Intent(MainActivity.this, NewConfiguration.class);
            startActivity(intent);
            return true;
        }

        if (id == R.id.item_02) {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            return true;
        }

//        if (id == R.id.item_03) {
//            Intent intent = new Intent(MainActivity.this, HelpDeskActivity.class);
//            startActivity(intent);
//            return true;
//        }

        if (id == R.id.item_03) {
            Intent intent = new Intent(MainActivity.this, About.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private long exitTime = 0;
    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(this, getResources().getString(R.string.msg_app_exist), Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        doExitApp();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        updateUI(Helper.checkInstalledConfig(MainActivity.this));
        super.onResume();
    }
}
